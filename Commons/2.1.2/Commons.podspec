#
# Be sure to run `pod lib lint Commons.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Commons'
  s.version          = '2.1.2'
  s.summary          = 'Commons classes available for BCIPersonas'
  s.swift_version    = '4.2'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/bancocreditoeinversiones/app-ios-pagos/src/develop'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ivan.galaz@bci.cl' => 'ivan.galazjeria@gmail.com' }
  s.source           = { :git => 'git@bitbucket.org:bancocreditoeinversiones/app-ios-commons.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.3'

  s.requires_arc = true

  s.dependency 'AEConsole', '0.6.1'
  s.dependency 'Alamofire', '4.7.1'
  s.dependency 'CryptoSwift', '0.13.0'
  s.dependency 'Dynatrace', '7.1.0.2043'
  s.dependency 'GoogleAnalytics', '3.17.0'
  s.dependency 'OHHTTPStubs', '6.0.0'
  s.dependency 'OHHTTPStubs/Swift', '6.0.0'
  s.dependency 'SwiftKeychainWrapper', '3.0.1'
  s.dependency 'SwiftyJSON', '4.0'
  s.dependency 'Swinject', '2.4.1'

  s.framework = 'CoreData', 'SystemConfiguration'
  s.libraries = 'GoogleAnalytics', 'z','stdc++','sqlite3'


  s.xcconfig =  { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
    'FRAMEWORK_SEARCH_PATHS' => '$(inherited) "${PODS_ROOT}"/**',
    'OTHER_LDFLAGS' => '$(inherited) -l"GoogleAnalytics"',
    'LIBRARY_SEARCH_PATHS' => '$(inherited) "${PODS_ROOT}"/**',
    'USER_HEADER_SEARCH_PATHS' => '$(inherited) "$(SRCROOT)/Pods/GoogleAnalytics"',
    'ENABLE_BITCODE' => 'YES'
  }

end

